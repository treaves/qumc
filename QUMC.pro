#-------------------------------------------------
#
# Project created by QtCreator 2018-08-14T13:37:51
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = QUMC
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

INCLUDEPATH += \
   $$PWD/src \
   $$PWD/src/boards \
   $$PWD/src/widgets \
   $$PWD/src/boards/ultimate


SOURCES += \
   src/main.cpp \
   src/MainWindow.cpp \
   src/LED.cpp \
   src/boards/ultimate/Ultimate.cpp \
   src/boards/ultimate/UltimateBoard.cpp \
   src/boards/Board.cpp \
   src/boards/LEDBoard.cpp \
   src/widgets/LEDWidget.cpp

HEADERS += \
   src/MainWindow.h \
   src/LED.h \
   src/boards/ultimate/Ultimate.h \
   src/boards/ultimate/UltimateBoard.h \
   src/boards/Board.h \
   src/boards/LEDBoard.h \
   src/widgets/LEDWidget.h

FORMS += \
   src/MainWindow.ui \
   src/boards/ultimate/Ultimate.ui \
   src/widgets/LEDWidget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
   resources/resources.qrc
