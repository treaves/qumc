#pragma once

#include "Board.h"
#include <QObject>

class LED;

class LEDBoard : public Board
{
   Q_OBJECT
public:
   explicit LEDBoard(QObject * parent = nullptr);

   virtual LED * ledAt(const int index) const = 0;
   virtual int ledCount(void) const = 0;

signals:

public slots:

};

