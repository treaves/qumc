#pragma once

#include <QObject>
#include <QMap>

#include "LEDBoard.h"

class LED;

class UltimateBoard : public LEDBoard
{
   Q_OBJECT
public:
   explicit UltimateBoard(QObject * parent = nullptr);

   virtual LED * ledAt(const int index) const override;
   virtual int ledCount(void) const override;

signals:

public slots:

private:
   QMap<int, LED *> leds;
};

