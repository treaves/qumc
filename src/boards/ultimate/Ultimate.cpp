#include "Ultimate.h"
#include "ui_Ultimate.h"

#include <QColor>
#include <QLabel>
#include <QScrollArea>
#include <QVBoxLayout>

#include "LED.h"
#include "UltimateBoard.h"
#include "LEDWidget.h"

Ultimate::Ultimate(QWidget * parent) :
    QWidget(parent),
    ui(new Ui::Ultimate)
{
    ui->setupUi(this);

    UltimateBoard * board  = new UltimateBoard(this);
    board->ledAt(1)->setColor(QColor::fromRgb(10,10,10));
    board->ledAt(1)->setStartPin(0);
    board->ledAt(2)->setColor(QColor::fromRgb(200,20,120));
    board->ledAt(2)->setStartPin(1);
    board->ledAt(3)->setColor(QColor::fromRgb(130,230,30));
    board->ledAt(3)->setStartPin(4);
    board->ledAt(4)->setColor(QColor::fromRgb(240,240,40));
    board->ledAt(4)->setStartPin(82);
    board->ledAt(5)->setColor(QColor::fromRgb(50,50,50));
    board->ledAt(5)->setStartPin(94);
    board->ledAt(6)->setColor(QColor::fromRgb(60,60,60));
    board->ledAt(6)->setStartPin(25);
    board->ledAt(7)->setColor(QColor::fromRgb(70,70,70));
    board->ledAt(7)->setStartPin(28);

    QWidget * central = new QWidget;
    QVBoxLayout * layout = new QVBoxLayout(central);
    layout->setSpacing(0);

    for (int x = 0; x < 32; x++) {
       layout->addWidget(new LEDWidget(x + 1, board->ledAt(x), central));
    }
    ui->scrollArea->setWidget(central);
    ui->scrollArea->setWidgetResizable(true);

}

Ultimate::~Ultimate()
{
    delete ui;
}
