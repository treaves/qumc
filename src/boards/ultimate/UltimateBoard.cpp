#include "UltimateBoard.h"

#include "LED.h"

UltimateBoard::UltimateBoard(QObject * parent) :
   LEDBoard(parent)
{
   for(int index = 0; index < 32; index++){
      leds[index] = new LED(this);
   }
}

LED * UltimateBoard::ledAt(const int index) const
{
   return leds[index];
}

int UltimateBoard::ledCount(void) const
{
   return leds.count();
}
