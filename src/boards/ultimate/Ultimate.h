#pragma once

#include <QWidget>

class LEDTableModel;

namespace Ui {
class Ultimate;
}

class Ultimate : public QWidget
{
    Q_OBJECT

public:
    explicit Ultimate(QWidget * parent = nullptr);
    ~Ultimate();

private:
    Ui::Ultimate * ui;
};


