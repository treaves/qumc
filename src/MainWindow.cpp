#include "MainWindow.h"
#include "ui_MainWindow.h"

#include "ultimate/Ultimate.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    Ultimate * ultimate = new Ultimate(this);
    ui->scrollArea->setWidget(ultimate);
}

MainWindow::~MainWindow()
{
    delete ui;
}
