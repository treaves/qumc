#pragma once

#include <QObject>
#include <QColor>

class LED : public QObject
{
   Q_OBJECT
   Q_PROPERTY(int     startPin READ startPin WRITE setStartPin NOTIFY startPinChanged)
   Q_PROPERTY(QColor  color    READ color    WRITE setColor    NOTIFY colorChanged)
   Q_PROPERTY(QString name     READ name     WRITE setName     NOTIFY nameChanged)

public:
    explicit LED(QObject * parent = nullptr);

   QColor color(void) const;
   void setColor(const QColor & color);
   QString name(void) const;
   void setName(QString name);
   /**
    * @brief startPin
    * The first pin for the LED.  A value of 0 means the pin has not been selected.
    * @return
    */
   int startPin(void) const;
   void setStartPin(const int pinNumber);

signals:
   void colorChanged(const QColor & newColor);
   void nameChanged(const QString & newName);
   void startPinChanged(const int newStartPin);

public slots:

private:
   QColor m_color;
   QString m_name;
   int m_startPin;
};

