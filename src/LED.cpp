#include "LED.h"

#include <QtDebug>

LED::LED(QObject * parent) :
   QObject(parent),
   m_color(Qt::black),
   m_name(""),
   m_startPin(0)
{

}

QColor LED::color(void) const
{
   return m_color;
}

void LED::setColor(const QColor & color)
{
    if (color.isValid()){
       this->m_color = color;
       emit colorChanged(color);
    }
}

QString LED::name(void) const
{
   return m_name;
}

void LED::setName(QString name)
{
   m_name = name;
}

int LED::startPin(void) const
{
   return m_startPin;
}

void LED::setStartPin(const int newStartPin)
{
   if((newStartPin - 1) % 3 == 0 || newStartPin == 0) {
      m_startPin = newStartPin;
      emit startPinChanged(newStartPin);
   }
}

