#ifndef LEDWIDGET_H
#define LEDWIDGET_H

#include <QWidget>

class LED;

namespace Ui {
class LEDWidget;
}

class LEDWidget : public QWidget
{
   Q_OBJECT

public:
   explicit LEDWidget(int ledNumber, LED * led, QWidget * parent = nullptr);
   ~LEDWidget();

signals:

private slots:
   void updateDisplayValues(void) const;
   void selectColor(void);
   void startPinSelected(void);
   void nameChanged(void);

private:
   Ui::LEDWidget * ui;
   LED * led;
   int ledNumber;
};

#endif // LEDWIDGET_H
