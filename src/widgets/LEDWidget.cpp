#include "LEDWidget.h"
#include "ui_LEDWidget.h"

#include <QColor>
#include <QColorDialog>
#include <QString>
#include <QtDebug>

#include "LED.h"
static const QString cssButtonColor = "QPushButton {"
    "background-color: %1;"
    "border-style: outset;"
    "border-radius: 2px;"
    "border-color: black;"
    "min-width: 2em;"
    "margin-left: 2px;"
    "padding-bottom: 4px;"
  "}"
  "QPushButton:pressed {"
    "background-color: rgb(200, 200, 200);"
    "border-style: inset;"
  "}";
LEDWidget::LEDWidget(int ledNumber, LED * led, QWidget *parent) :
   QWidget(parent),
   ui(new Ui::LEDWidget),
   led(led),
   ledNumber(ledNumber)
{
   ui->setupUi(this);
   ui->label->setText(tr("LED %1").arg(ledNumber));
   ui->selectColor->setToolTip(tr("Select Color"));
   ui->clearColor->setToolTip(tr("LED off"));
   ui->comboBoxStartPin->setToolTip(tr("Select LED start pin"));
   ui->lineEditLEDName->setToolTip(tr("Enter a name for this LED"));
   ui->lineEditLEDName->setText(led->name());

   ui->comboBoxStartPin->addItem(QString("Unset"), QVariant::fromValue(0));
   for(int index = 1; index < 95; index+=3){
      ui->comboBoxStartPin->addItem(QString("%1 - %2").arg(index).arg(index + 2), QVariant::fromValue((index)));
   }
   if(led->startPin() > 0) {
      ui->comboBoxStartPin->setCurrentIndex(((led->startPin() - 1) / 3) + 1);
   }

   updateDisplayValues();
   connect(ui->selectColor, SIGNAL(clicked()), this, SLOT(selectColor()));
   connect(ui->comboBoxStartPin, SIGNAL(currentTextChanged(const QString &)), this, SLOT(startPinSelected()));
   connect(ui->lineEditLEDName, SIGNAL(editingFinished()), this, SLOT(nameChanged()));
   connect(led, SIGNAL(colorChanged(const QColor &)), this, SLOT(updateDisplayValues()));
}

LEDWidget::~LEDWidget()
{
   delete ui;
}

void LEDWidget::selectColor(void)
{
   const QColor color = QColorDialog::getColor(led->color(), this, "Select Color");

   if (color.isValid()) {
      led->setColor(color);
   }
}

void LEDWidget::updateDisplayValues(void) const
{
   QString qss = QString(cssButtonColor).arg(led->color().name());
   ui->selectColor->setStyleSheet(qss);
}

void LEDWidget::startPinSelected(void)
{
   led->setStartPin(ui->comboBoxStartPin->currentData().toInt());
}

void LEDWidget::nameChanged(void)
{
   led->setName(ui->lineEditLEDName->text());
}
